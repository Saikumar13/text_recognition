from libraries import *
import time
from spacy.matcher import PhraseMatcher
import spacy

from model_utils import CRAFT, copyStateDict, RefineNet
from prediction_utils import test_net, demo
from file_utils import CTCLabelConverter, AttnLabelConverter
from model_utils import Model
#project_path = os.getcwd() + '/'
class load_models(object):
    def __init__(self):

        self.default_kwargs = {'trained_model': project_path + 'Pretrained_Models/craft_mlt_25k.pth',
                          'basic':False,'use_gpu': False, 'refine': False, 'refiner_model': project_path+'Pretrained_Models/craft_refiner_CTW1500.pth',
                          'text_threshold': 0.7, 'low_text': 0.4, 'link_threshold': 0.4, 'canvas_size': 1280,
                          'mag_ratio': 1.5,'poly': False, 'show_time': False, 'test_folder': project_path + '/Frame_vid',
                          'image_folder': project_path + 'Cropped_image', 'workers': 4, 'batch_size': 192,
                          'CRNN':True,
                          'saved_model':project_path + 'Pretrained_Models/CRNN-PyTorchCTC.pth',
                          #'saved_model': project_path + 'Pretrained_Models/TPS-ResNet-BiLSTM-Attn-case-sensitive.pth',
                          'batch_max_length': 25, 'imgH': 32, 'imgW': 100,
                          'character': '0123456789abcdefghijklmnopqrstuvwxyz','sensitive': True, 'rgb': False, 'PAD': False,
                          'Transformation': 'None','FeatureExtraction': 'CRNN','SequenceModeling': 'None', 'Prediction': 'CTC',
                          #'Transformation': 'TPS','FeatureExtraction': 'ResNet','SequenceModeling': 'BiLSTM', 'Prediction': 'Attn',
                          'num_fiducial': 20, 'input_channel': 1,'output_channel': 512,'hidden_size': 256, 'debug': False
                          } 

    def recognition(self,**passed_kwargs):
        opt = self.default_kwargs.copy()
        opt.update(passed_kwargs)

        if opt['basic']:
            if opt['CRNN']:
                print("Inside CRNN")
                opt['sensitive']=False
                opt['saved_model']=project_path + 'Pretrained_Models/CRNN-PyTorchCTC.pth'
                opt['Transformation'] = 'None'
                opt['FeatureExtraction']='RCNN'
                opt['SequenceModeling']='None'
                opt['Prediction']='CTC'
            else:
                print("Inside Basic")
                opt['saved_model'] = project_path + 'Pretrained_Models/None-VGG-None-CTC.pth'
                opt['sensitive']=False
                opt['Transformation'] = 'None'
                opt['FeatureExtraction']='VGG'
                opt['SequenceModeling']='None'
                opt['Prediction']='CTC'
        else:
            opt['saved_model']=project_path + 'Pretrained_Models/TPS-ResNet-BiLSTM-Attn-case-sensitive.pth'
            opt['sensitive']=True
            opt['Transformation'] = 'TPS'
            opt['FeatureExtraction']='ResNet'
            opt['SequenceModeling']='BiLSTM'
            opt['Prediction']='Attn'
        if opt['sensitive']:
            import string
            opt['character'] = string.printable[:-6]  # same with ASTER setting (use 94 char).

        cudnn.benchmark = True
        cudnn.deterministic = True
        #opt['num_gpu'] = torch.cuda.device_count()
        opt['num_gpu']=0
        # model configuration
        if 'CTC' in opt["Prediction"]:
            converter = CTCLabelConverter(opt["character"])
        else:
            converter = AttnLabelConverter(opt["character"])
        opt["num_class"] = len(converter.character)

        if opt['use_gpu']:
            device = torch.device('cuda')
            opt['num_gpu'] = torch.cuda.device_count()
        else:
            device = torch.device('cpu')

        if opt["rgb"]:
            opt["input_channel"] = 3
        model = Model(opt)

        model = torch.nn.DataParallel(model).to(device)

        # load model
        print('loading pretrained model from %s' % opt["saved_model"])
        model.load_state_dict(torch.load(opt["saved_model"], map_location=device))
        print("---Model Loaded ---")

        return model,device,converter

    def craft(self,**passed_kwargs):
        kwargs = self.default_kwargs.copy()
        kwargs.update(passed_kwargs)
        # --------------------- Loading the model ------------------------------
        try:
            net = CRAFT()  # intialising the model architechture
            print('Loading weights from checkpoint (' + kwargs['trained_model'] + ')')

            if kwargs['use_gpu']:
                net.load_state_dict(copyStateDict(torch.load(kwargs['trained_model'])))
            else:
                net.load_state_dict(copyStateDict(torch.load(kwargs['trained_model'], map_location='cpu')))

            if kwargs['use_gpu']:
                net = net.cuda()
                net = torch.nn.DataParallel(net)
                cudnn.benchmark = False

            net.eval()

            # LinkRefiner
            refine_net = None
            if kwargs['refine']:
                refine_net = RefineNet()
                print('Loading weights of refiner from checkpoint (' + kwargs['refiner_model'] + ')')
                if kwargs['use_gpu']:
                    refine_net.load_state_dict(copyStateDict(torch.load(kwargs['refiner_model'])))
                    refine_net = refine_net.cuda()
                    refine_net = torch.nn.DataParallel(refine_net)
                else:
                    refine_net.load_state_dict(
                        copyStateDict(torch.load(kwargs['refiner_model'], map_location=torch.device('cpu'))))
                    refine_net.eval()
                    kwargs['poly'] = True
            return net, refine_net

        except Exception as er:
            print("Error occured while Loading the model")
            print("Error is ", er)
            sys.exit(1)

    def load_regex_model(self,basic=False):
        nlp = spacy.load("en_core_web_sm")
        matcher = PhraseMatcher(nlp.vocab)
        if basic:
            phn_pt = ['1800 734 2342', '800 660 3500', '800 6600 350',
                      '8100 660 350', '18007341232', '8006603500']
            vanity_pt = ['877462fita', 'i4323liberty', '1800g0start', '11800emaglity',
                         '18004liberty', '833emaglity', '122123emaglity']
        else:
            phn_pt = ['1800-111-1111', '1-800-349-5922', '1800 734 2342', '(1-123-213-1233)', '1800-999 1234',
                      '1-800 999 1234', '1-800-999 1234',
                      '800-660-3500', '800 660-3500', '800-660 3500', '800 660 3500', '800-6600-350', '800 6600 350',
                      '800-6600 350',
                      '800-6600-350', '800 6600-3500', '8100-660-350', '8100 660 350', '301104970-971', '800-6600350',
                      '(800)999 2341', '(800) 999 2341', '(8000) 999 234', '(855) 915-5315', '112 123 1938',
                      '18007341232', '(855)-915 5315', '915 5213 (855)', '915 5213-(855)', '9152-521-(855)',
                      '915-5213-(855)', '915-(5213)-855', '(915)-2123 855',
                      '1-866-44-7762', '(800) 481 -1 700', '1-866-44-7762']

            vanity_pt = ['1-823emaglity', '1-833 emaglity', '1-800-4-emaglity', '877-462-fita', 'i4323liberty',
                         '1-800-g0-start', '1-1800-emaglity', '1-1800-em0glity', '(1-1800)-emaglity', '(1800)-emaglity',
                         '(1800) emaglity', '(1800)emaglity', '1-800-4-liberty', '1-833-emaglity', '833-emaglity',
                         '182-123 emaglity',
                         '1-800 4-liberty', '844 654 trin', '1-b00-progressive', '877-713-fit1', '1-877-go-u-haul',
                         '1-800-jenny20','800-phone-sexy','800-phone-sex','1-800-PIT-SHOP']
        phn_patterns = [nlp(term) for term in phn_pt]
        vanity_patterns = [nlp(v) for v in vanity_pt]
        phn_matcher = PhraseMatcher(nlp.vocab, attr="SHAPE")
        vanity_matcher = PhraseMatcher(nlp.vocab, attr="SHAPE")
        phn_matcher.add("phonenumber_matching", None, *phn_patterns)
        vanity_matcher.add("vanityPhone_matching", None, *vanity_patterns)

        return nlp,phn_matcher,vanity_matcher

