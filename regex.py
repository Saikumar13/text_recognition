#!pip install spacy
import json
import re
# the below 2 lines are required to extract the data

# T0-DO
"""
the check should be 1. phone numbers on same line; 2. gap between chunks should not be more than avg size of each letter.
how to get avg width of each number : divide W of number block by number of letters
You can be relaxed about confidence id 1. size is good, and 2.gaps between number sections are small

"""
#Function to filter out wrong numbers

def filter_tfn(input_tfn_list):
    out_tfn_list=[]
    for input_tfn in input_tfn_list:
        tfn=''.join(e for e in input_tfn if e.isalnum())
        if 9<len(tfn)<=11:
            if tfn.startswith('18'):
                out_tfn_list.append(input_tfn)
                #process internation number
            elif tfn.startswith('1') or tfn[3]=='1' or (tfn[4]=='1' and tfn[5]=='1'):
                #invalid number 
                pass
            else:
                out_tfn_list.append(input_tfn)
    return out_tfn_list

#function to filter numbers based on location
def tfnLocationBasedFilter(df,numbrs):
    try:
        flt_nums=[]
        for num in numbrs:
            same_Y=True
            locations=[]
            for pt in num.split(' '):
                for b in df['fullTextAnnotation']:
                    if pt in b['text']:
                        #print(b)
                        locations.append(b)
            if len(locations)>=2:
                for lc in range(1,len(locations)):
                    prev_loc=locations[0]['boundingBox']['Y']
                    cur_loc=locations[lc]['boundingBox']['Y']
                    thresh = max(locations[0]['boundingBox']['H'] , locations[lc]['boundingBox']['H'] ) / 3.0
                    #print(thresh)
                    if prev_loc-thresh<=cur_loc<=prev_loc+thresh:
                        pass
                        #print(prev_loc,cur_loc)
                        #print('inside final check')
                        #flt_nums.append(num)
                    else:
                        same_Y=False
                        break
            else:
                pass
                #flt_nums.append(num)
            if same_Y:
                flt_nums.append(num)
            else:
                pass
                
        return flt_nums
    except Exception as er:
        print("Error occured while filtering Nums based on location error is: ",er)
        return er



# function to extract numbers
def extract_numbers(doc,nlp,phn_matcher,vanity_matcher):
    # for numbers
    numbers = []
    vanity_numbers = []
    doc = doc.replace('.', '').replace(',', '-').replace('--', '-')
    doc = re.sub(r"-\s+|\s+-\s+", "-", doc)
    #replacing the extra spaces before - symbol
    doc = re.sub(r"\s+-|\s+-\s+", "-", doc)
    doc = nlp(doc)

    phone_matches = phn_matcher(doc)
    vanity_matches = vanity_matcher(doc)
    # Phone Matched items
    nms = ""
    for match_id, start, end in phone_matches:
        span = doc[start:end]
        if not str(span.text) in nms:
            cleaned_num=re.sub('\D', '',str(span.text))
            if len(cleaned_num) > 8 and len(cleaned_num) < 12:
                if str(span.text).startswith('1-3'):
                    numbers.append(str(span.text).replace('1-3','1-8'))
                else:
                    numbers.append(str(span.text))

            else:
                pass
        else:
            pass
        nms = nms + " " + str(span.text)
    # vanityPhone matches
    for match_id, start, end in vanity_matches:
        span = doc[start:end]
        clean_vnum=str(span.text).replace('-','').replace('(','').replace(' ','')
        if len(clean_vnum) > 9 and (clean_vnum.startswith('18') or clean_vnum.startswith('8') or clean_vnum.startswith('1b')) and 'reviews' not in str(span.text) and 'likes' not in str(span.text):
            if str(span.text).startswith('1b'):
                vanity_numbers.append('18'+str(span.text)[2:])
            elif str(span.text).startswith('1-b'):
                vanity_numbers.append('1-8'+str(span.text)[3:])
            else:
                vanity_numbers.append(str(span.text))
    return numbers, vanity_numbers


# function to exteact urls and text and downloadable
def extract_data(query,nlp):
    query = re.sub(' +', ' ', query).lower().replace('..', '.')
    sents = nlp(query)
    # removing stop words and puncutations(!?,etc)
    stopwords_puncts = [token for token in sents if not token.is_stop and not token.is_punct]
    # print(stopwords_puncts)
    # for Lemmatizing
    # token.lemma_
    # emails=[]
    urls = []
    downloadable = "No"
    text = []
    domains = ['com', 'edu', 'in', 'us', 'gov', 'tv', 'org', 'info', 'co', 'ecom', 'eu', 'net']
    special_chars = list('.@/:,";!#$%^&*(){}[]?\|-_+=`~>< ')
    try:
        for i in range(len(sents)):
            if sents[i] in stopwords_puncts:
                # print(sents[i])
                if sents[i].like_url:
                    if any(x for x in domains if str(sents[i].text).endswith(x)) and '@' not in str(sents[i].text):
                        # urls[str(sents[i].text)]=int(scores_dict.get(str(sents[i].text),0)*100)
                        urls.append(sents[i].text)
                    else:
                        pass
                elif any(x for x in domains if '.' + x in str(sents[i].text)) and len(str(sents[i].text)) > 5:
                    if str(sents[i].text).startswith(tuple(domains)) or str(sents[i].text).startswith(tuple(['.'+i for i in domains])):
                        if len(str(sents[i-1].text))+len(str(sents[i].text))>5 and '@' not in str(sents[i-1].text)+str(sents[i].text):
                            urls.append(str(sents[i-1].text)+str(sents[i].text))
                        else:
                            pass
                    else:
                        if '@' not in str(sents[i].text) and ( any(x for x in domains if str(sents[i].text).endswith(x)) or '/' in  str(sents[i].text) ):
                            urls.append(sents[i].text)
                        else:
                            pass

                elif str(sents[i]) in domains or str(sents[i]) in ['.' + d for d in domains] and str(sents[i]) is not None:
                    # urls[str(sents[i-1])+"."+str(sents[i])]=int(scores_dict.get(str(sents[i].text))*90)
                    if str(sents[i - 1]) in special_chars or len(str(sents[i - 1])) <= 1 or str(sents[i - 1]) in domains:
                        if len(str(sents[i - 2])) + len(str(sents[i]))>5 and '@' not in str(sents[i-2].text)+str(sents[i].text):
                            if '.' in str(sents[i]):
                                urls.append(str(sents[i - 2]) + str(sents[i]))
                            else:
                                urls.append(str(sents[i - 2]) + "." + str(sents[i]))
                        else:
                            pass
                    else:
                        if len(str(sents[i - 1])) + len(str(sents[i]))>5 and '@' not in str(sents[i-1].text)+str(sents[i].text):
                            if '.' in str(sents[i]):
                                urls.append(str(sents[i - 1]) + str(sents[i]))
                            else:
                                urls.append(str(sents[i - 1]) + "." + str(sents[i]))
    except Exception as er:
        pass
    if "download" in query or "app store" in query:
        downloadable = "Yes"
    regex = re.compile('text * ([0-9]*)')
    text.extend(regex.findall(query))
    # print("numbers",nums,"\nwebsites",urls,"\ndownload-able:",downloadable,'\ntext Numbers',text,'\nVanity Numbers',vanity_numbers)
    return  urls, text, downloadable


def translate_number(vanity_phone_number):
    vanity_phone_number.replace('-', '')
    temp = re.sub(r"[abcABC]", "2", vanity_phone_number)
    temp = re.sub(r"[defDEF]", "3", temp)
    temp = re.sub(r"[ghiGHI]", "4", temp)
    temp = re.sub(r"[jklJKL]", "5", temp)
    temp = re.sub(r"[mnoMNO]", "6", temp)
    temp = re.sub(r"[pqrsPQRS]", "7", temp)
    temp = re.sub(r"[tuvTUV]", "8", temp)
    temp = re.sub(r"[wxyzWXYZ]", "9", temp)
    temp = re.sub(r"-", "", temp)
    temp = '%s-%s-%s' % (temp[0:3], temp[3:6], temp[6:10])
    return temp


def refine_vanityNum(phone_number, basic):
    if phone_number is not None:
        if basic:
            alphabets ="abcdefghijklmnopqrstuvwxyz"
            if phone_number[0] in alphabets:
                phone_number = phone_number[1:]
        else:
            phone_number = phone_number.replace('.', '-')
            phone_number = re.sub(r"[,()_ ]", "", phone_number)
            if phone_number[0:2] in ['1-', 'I-', 'l-']:
                phone_number = phone_number[2:]
        temp = phone_number.replace('-', '')
        vanity_phone_number = None
        if not temp.isdigit():
            vanity_phone_number = phone_number
            phone_number = translate_number(vanity_phone_number)
        return phone_number


import collections


def sort_dict(dt):
    # ordering by y &x
    sorted_list = sorted(dt, key=lambda k: [dt[k]['co-ords'][1], dt[k]['co-ords'][0]], reverse=False)
    srt_dict = collections.OrderedDict({k: dt[k] for k in sorted_list})
    #print("Srt dict: ",srt_dict)
    input_array = list(v['co-ords'] for v in srt_dict.values())
    output_array = []
    previous_row = []
    for block in input_array:
        #print(block)
        if previous_row == []:
            previous_row.append(block)
        else:
            if block[1] + block[3] < previous_row[-1][1] + 1.50 * previous_row[-1][3]:
                previous_row.append(block)
            else:
                if block[1] > previous_row[-1][1] + previous_row[-1][3] or block[1] + block[3] > previous_row[-1][1] + \
                        previous_row[-1][3] * 1.50:
                    output_array.append(sorted(previous_row, key=lambda k: k[0]))
                    previous_row = []
                    previous_row.append(block)
    output_array.append(sorted(previous_row, key=lambda k: k[0]))  # sorted x row
    #print("Output array: ",output_array)
    srt_list = []
    #print("Sort dict: ",srt_dict.items())
    for row in output_array:
        for i in row:
            #print("i",i)
            for key, value in srt_dict.items():
                #print("Key: ",key,"Value: ",value)
                if key:
                    if value['co-ords'] == i:
                        #print(i,key)
                        #0.35 for 3 letters going down to 0.15 for > 8 letters
                        if len(str(key))<=3:
                            conf_threshold=0.3
                        elif 3<len(str(key))<8:
                            conf_threshold=0.2
                        else:
                            conf_threshold=0.1
                        if float(value['confidence'])>float(conf_threshold):
                            srt_list.append(key)
                        else:
                            pass
                #if no text
                else:
                    return ''
    return (" ".join([i.split('_')[0] if '_' in i else i for i in srt_list]))


# extracts data and refines (calls all the above functions)
def extract_info(query,df,basic,nlp,phn_matcher,vanity_matcher):
    try:
        urls, text, downloadable = extract_data(query,nlp)
        nums, vanity_numbers = extract_numbers(query,nlp,phn_matcher,vanity_matcher)
        #refine numbers
        nums=filter_tfn(nums)
        nums=tfnLocationBasedFilter(df,nums)
        if vanity_numbers:
            for num in vanity_numbers:
                nums.append(refine_vanityNum(num, basic))
        return nums, urls, text, downloadable, vanity_numbers
    except Exception as er:
        print("error occured while doing Regex\nerror is ", er)


