from dict2xml import dict2xml
import json
import pandas as pd
import os
import ast
from pascal_voc_writer import Writer
import functools
# import pandas as pd
from PIL import Image, ImageDraw, ImageFont
import csv
import pandas as pd
import logging
import traceback

'''
Util function for Z algo. 
Fills Z array for given string str[] 
'''


def getZarr(string, z):
    len_string = len(string)

    left, right, k = 0, 0, 0
    for i in range(1, len_string):

        if i > right:
            left, right = i, i

            while right < len_string and string[right - left] == string[right]:
                right += 1
            z[i] = right - left
            right -= 1
        else:

            k = i - left

            if z[k] < right - i + 1:
                z[i] = z[k]

            else:

                left = i
                while (right < len_string) and (string[right - left] == string[right]):
                    right += 1
                z[i] = right - left
                right -= 1


'''
String matching 'Z' algorithm
'''


def search(text, pattern):
    # Create concatenated string "P$T"
    concat = pattern + "$" + text
    l = len(concat)

    # Construct Z array
    z = [0] * l
    getZarr(concat, z)

    for i in range(l):

        if z[i] == len(pattern):
            print("Pattern found at index", i - len(pattern) - 1)
            return True

    return False


'''
Find the item which has maximum occurrence in the dictionary
'''


def find_Max_Voted_Item(dict_of_all_items):
    max_voted_item = ''
    max_votes_for_item = 0

    for item in dict_of_all_items:

        count_of_item = dict_of_all_items[item]['count']

        if (max_votes_for_item < count_of_item):
            max_voted_item = item
            max_votes_for_item = count_of_item

    return max_voted_item, max_votes_for_item


'''
Find total area for a number or a url which is spanned across more than 1 bounding box
'''


def findArea(foundStartIndex, foundEndIndex, annotated_Data):
    totalArea = 0

    for index_of_bbox in range(foundStartIndex, foundEndIndex):

        # print(index_of_bbox, len(annotated_Data))

        if (index_of_bbox < len(annotated_Data)):
            height = int(annotated_Data[index_of_bbox]['boundingBox']['H'])
            width = int(annotated_Data[index_of_bbox]['boundingBox']['W'])
            totalArea += (height * width)

    length = (foundEndIndex - foundStartIndex)

    return totalArea / length


'''
Find aggregate confidence if a number or a url is spanned across more than 1 bounding box
'''


def findAggConfidence(foundStartIndex, foundEndIndex, annotated_Data):
    totalConfidence = 0.0

    for index_of_bbox in range(foundStartIndex, foundEndIndex):

        # print(annotated_Data[index_of_bbox]['confidence'])
        if (index_of_bbox < len(annotated_Data)) and len(annotated_Data[index_of_bbox]['confidence']):
            confidence = float(annotated_Data[index_of_bbox]['confidence'])
            totalConfidence += confidence

    length = (foundEndIndex - foundStartIndex)

    return totalConfidence / length


'''
Find the matching index of the item in Concatenated text 
'''


def findIndex_OfMatch(item, concat_Text):
    '''
    Split the no. from space (300 1866 223)
    In this case, We will have to match the first split text and then assume
    that the next boxes in item(url, number, vanity number) are matching with the next boxes of concatenated text
    '''

    splitted_text = item.split(' ')
    splitted_ConcatText = concat_Text.split(' ')

    '''
    Check for number in this first block or If url is a direct match.
    '''
    foundIndex = -1
    for index, substring in enumerate(splitted_ConcatText):

        # print(splitted_number[0], substring)

        if substring.lower() == splitted_text[0]:
            foundIndex = index
            # print('\t\t------MATCH FOUND-----', foundIndex, substring, splitted_ConcatText[foundIndex])

            return index, index + len(splitted_text)

    '''
    Split the number from '.' to find a match for url
    '''
    if (foundIndex == -1):

        splitted_text = item.split('.')

        for index, substring in enumerate(splitted_ConcatText):

            if substring.lower() == splitted_text[0]:
                foundIndex = index

                return index, index + len(splitted_text)

    return foundIndex, foundIndex + len(splitted_text)


'''
Find position is appending the positions just now.
Todo : Have to merge both the bounding boxes
'''


def findPositions(foundStartIndex, foundEndIndex, annotated_Data):
    positions_list = list()
    for index_of_bbox in range(foundStartIndex, foundEndIndex):

        if index_of_bbox < len(annotated_Data):
            positions_list.append(annotated_Data[index_of_bbox]['boundingBox'])

    return positions_list


'''
Main function to find the area of an item.
Calls other utility functions from inside
'''


def find_AreaConfidenceMatchfound_ofItem(list_item, all_items_dict, concat_Text, annotated_Data):
    for item in list_item:

        '''
        If the number is found in concatText, Compute the area of entire number.

        Since an item can be spanned over more than 1 bounding box then we will have 
        to find the area, confidence for all the bounding boxes.
        '''
        startIndex, endIndex = findIndex_OfMatch(item, concat_Text)

        if (startIndex != -1):

            '''
            Find the area of an item which has been matched
            '''
            totalArea = findArea(startIndex, endIndex, annotated_Data)

            if 'area' in all_items_dict[item].keys():
                all_items_dict[item]['area'] += totalArea
            else:
                all_items_dict[item]['area'] = totalArea

            # print('--TOTAL AREA--', totalArea)
            if 'max_area' in all_items_dict[item].keys():
                all_items_dict[item]['max_area'] = max(totalArea, all_items_dict[item]['max_area'])
            else:
                all_items_dict[item]['max_area'] = totalArea

            # -----------------------------------------------------

            '''
            Find the confidence of an item which has been matched.
            '''
            confidence = findAggConfidence(startIndex, endIndex, annotated_Data)

            if 'max_confidence' in all_items_dict[item].keys():
                all_items_dict[item]['max_confidence'] = max(confidence, all_items_dict[item]['max_confidence'])
            else:
                all_items_dict[item]['max_confidence'] = confidence

            if 'confidence' in all_items_dict[item].keys():
                all_items_dict[item]['confidence'] += confidence
            else:
                all_items_dict[item]['confidence'] = confidence

            # -----------------------------------------------------

            positions_list = findPositions(startIndex, endIndex, annotated_Data)
            if 'position_list' in all_items_dict[item].keys():
                all_items_dict[item]['position_list'] += (positions_list)
            else:
                all_items_dict[item]['position_list'] = positions_list

            '''
            We have to take the aggregate of the confidence and area.
            So, To find the aggregate we need the number of matches found at last.
            '''
            if 'match_found' in all_items_dict[item].keys():
                all_items_dict[item]['match_found'] += 1
            else:
                all_items_dict[item]['match_found'] = 1

        # -------------------------------------------------------


'''
Convert the string from the predictions to list.
To make a unique list, put it into set and then again switch to list.
'''


def convert_stringItem_toList(item):
    item_toList = ast.literal_eval(item)
    list_set = set(item_toList)
    unique_itemToList = list(list_set)

    return unique_itemToList


'''
Update the count of item in the list.
Count is different than the match found. 
'''


def update_count_ofItem(item_list, all_items_dict,frame_timestamp):
    for item in item_list:

        if item in all_items_dict.keys():

            all_items_dict[item]['count'] += 1
            all_items_dict[item]['frame_timestamp']=frame_timestamp
        else:
            all_items_dict[item] = {'count': 1}
            all_items_dict[item]['frame_timestamp']=frame_timestamp



'''
Check if the item is present in previous ad or not.
To handle the cases where transition of ads can cause the detection 
of previous ad's entity in the current ad. 
So, Just have to check for starting of the ad i.e. 0-2 frames.
'''


def check_if_present_in_previousAd(list_item, previous_ad_item):
    for item in list_item:

        if (previous_ad_item != None):

            print('--PREVIOUS AD ITEM --', previous_ad_item, item)
            if (search(item.lower(), previous_ad_item.lower()) or search(previous_ad_item.lower(), item.lower())):
                list_item.remove(item)
                print('--- REMOVING ITEM --- ', item)


'''
If 'position' coordinate list is present already in count_dict then increment the count.
Otherwise return false, So that we can put this list in our dict.
'''


def update_if_presentInWindow(position, coordinate_count_dict, window=10):
    x_coordinate = position['X']
    y_coordinate = position['Y']

    for item in coordinate_count_dict:

        '''
        Each tuple in coordinate_count_Dict has (x,y,h,w)
        0th index -> X coordinate of bb
        1st index -> Y coordinate of bb
        2nd index -> height of bb
        3rd index -> width of bb
        '''
        bottomValueX = int(item[0]) - window
        upperValueX = int(item[0]) + window
        bottomValueY = int(item[1]) - window
        upperValueY = int(item[1]) + window

        if (upperValueX > x_coordinate > bottomValueX) and (upperValueY > y_coordinate > bottomValueY):
            coordinate_count_dict[item] += 1
            return True

    return False


'''
Find the dominant position of the bounding box.
Dominant i.e. Occurring max times within a window of x and y coordinates.
'''


def find_DominantPosition(all_items_dict):
    '''
    Count the number of times coordinates have occurred.
    '''
    coordinate_count_dict = dict()

    for item in all_items_dict:

        if 'position_list' in all_items_dict[item].keys():

            # print("--HERE--", all_items_dict)
            position_list = all_items_dict[item]['position_list']

            for position in position_list:

                # print(position)
                x_coordinate = position['X']
                y_coordinate = position['Y']
                height = position['H']
                width = position['W']

                '''
                If there is atleast one position list present in coord_count_dict 
                i.e. It has been processed atleast once.
                So, Check if this position list lier within any position list present in our dict already.
                '''
                if len(coordinate_count_dict):

                    '''
                    Put the list in count dict if it doesn't lies inside any of the coordinate list already present.
                    '''
                    if (update_if_presentInWindow(position, coordinate_count_dict) == False):
                        position_list = (position['X'], position['Y'], position['H'], position['Y'])
                        coordinate_count_dict[position_list] = 1
                else:
                    '''
                    If we have not processed any list of coordinates then just count this one as 1.
                    '''
                    position_list = (position['X'], position['Y'], position['H'], position['Y'])
                    coordinate_count_dict[position_list] = 1


# print('--- COORDINATE COUNT DICT ----------',coordinate_count_dict)


'''
Find score of the item. 
'''


def find_MaxScoreItem(all_items_dict):
    max_score = 0
    max_score_item = None
    best_timestamp = None
    score_list=[]

    # print(all_items_dict)
    for item in all_items_dict:

        if 'count' in all_items_dict[item].keys():
            count = all_items_dict[item]['count']
        else:
            count = 0.0

        if 'area' in all_items_dict[item].keys():
            area = all_items_dict[item]['area']
        else:
            area = 0.0

        if 'confidence' in all_items_dict[item].keys():
            confidence = all_items_dict[item]['confidence']
        else:
            confidence = 0.0

        if 'match_found' in all_items_dict[item].keys():
            match_found = all_items_dict[item]['match_found']
        else:
            match_found = 0.0

        if 'max_confidence' in all_items_dict[item].keys():
            max_confidence = all_items_dict[item]['max_confidence']
        else:
            max_confidence = 0.0

        if 'max_area' in all_items_dict[item].keys():
            max_area = all_items_dict[item]['max_area']
        else:
            max_area = 0.0

        # print(max_area)
        # if (match_found > 0) :
        # 	area = area/match_found
        # 	# confidence = confidence/match_found
        # 	print(area)

        score = (0.5 * count) + (max_confidence)
        score_list.append(score)
        # print('\t\t---Item {}, Score {} '.format(item, score) )
        if score > max_score:
            max_score_item = item
            max_score = score
            best_timestamp =all_items_dict[item]['frame_timestamp']
    if sum(score_list)==0:
        max_score=max_score
    else:
        max_score=max_score / sum(score_list)
    return max_score_item,best_timestamp,max_score

#check if text is present in middle of the frame
def check_if_text_inMiddle(annotation,range=[180,300,300,420]):

    in_centre=False
    is_small=True
    for word in annotation:
        cords=word['boundingBox']
        X1=cords['X']
        Y1=cords['Y']
        X2=X1+cords['W']
        Y2=Y1+cords['H']
        if range[0]<X1<range[1] and range[2]<Y1<range[3]:
            if cords['H']>20 and cords['W']>40:
                is_small=False
            in_centre=True
    return in_centre,is_small


#scoring Function
def get_score(final_dict):
    score=0
    data_count=1
    if final_dict['downloadable'] == 'yes':
        downloadable=1
    else:
        downloadable=0

    data_values=[final_dict['urls_data']['confidence'],final_dict['numbers_data']['confidence'],final_dict['vanityNumbers_data']['confidence'],downloadable]
    data_count=sum(i!=0 for i in data_values)
    if data_count==0:
        data_score=sum(data_values)
    else:
        data_score=1
        #data_score=sum(data_values)/data_count

    if final_dict['Text_In_Centre']:
        if final_dict['is_small']:
            score=score+0.5
        else:
            score = score + 1.0
    else:
        score=score+0.25
    score=(0.75*score)+(1*data_score)
    #print("data score :",data_score,"\n data count is: ",data_count,"\n data values: ",data_values)
    #print("actual score is : ",round(score/1.75,3))
    return round(score/1.75,4)


def main(data):
    # previous_ad_url, previous_ad_num, previous_ad_vanity = None,None,None
    details_to_return = dict()
    all_urls_forAd = dict()  # To store every item.
    all_nums_forAd = dict()
    all_vanityNum_forAd = dict()
    for frame in data:
        # Helper variables.
        # details = None
        downloadableFlag = 0
        #print(frame)
        frame_timestamp = data[frame]['timestamp']
        #print(frame_timestamp)

        # Pre-Processing
        '''
           Get needful data from the json
         '''
        # details = data[frame]['details']
        concat_Text = data[frame]['predicted_text']['ConcatText']
        annotated_Data = data[frame]['predicted_text']['fullTextAnnotation']

        #check if there is text present in centre
        in_centre,is_small=check_if_text_inMiddle(annotated_Data)
        # print(annotated_Data)
        # Take the predicted strings
        url = data[frame]['urls']
        number = data[frame]['nums']
        vanity_number = data[frame]['vanity_numbers']
        # print(number)
        # If downloadable is 'yes' for even a single frame then put it's flag '1'
        downloadable = data[frame]['downloadable']
        if (downloadable.lower()) == 'yes':
            # print('--DOWNLOADABLE--',downloadable.lower())
            downloadableFlag = 1

        # Convert the predicted data to list from string.
        # list_url = convert_stringItem_toList(url)
        list_url = url
        list_number = number
        list_vanity_number = vanity_number

        # Update the count of the url, number, vanity_number in the list that is prepared for an Ad.

        if len(list_url):
            # check_if_present_in_previousAd(list_url, previous_ad_url)
            update_count_ofItem(list_url, all_urls_forAd,frame_timestamp)
        #print("urls: ", all_urls_forAd)

        if len(list_number):
            '''
                Check if the number was in the previous ad or not.
            '''
            # check_if_present_in_previousAd(list_number, previous_ad_num)
            update_count_ofItem(list_number, all_nums_forAd,frame_timestamp)

        if len(list_vanity_number):
            '''
                Check if the vanity was in the previous ad or not.
            '''
            # check_if_present_in_previousAd(list_vanity_number, previous_ad_vanity)
            update_count_ofItem(list_vanity_number, all_vanityNum_forAd,frame_timestamp)
            '''
            Area of a single number for now.
            For more than 1 numbers check videos first, since numbers are in string 
            Will have to check whether how many digits numbers are there
            '''
        areaOfNumbers = find_AreaConfidenceMatchfound_ofItem(list_number, all_nums_forAd, concat_Text, annotated_Data)
        areaOfURL = find_AreaConfidenceMatchfound_ofItem(list_url, all_urls_forAd, concat_Text, annotated_Data)
        areaOfVanity = find_AreaConfidenceMatchfound_ofItem(list_vanity_number, all_vanityNum_forAd, concat_Text,
                                                            annotated_Data)

    # DEV MODE
    find_DominantPosition(all_urls_forAd)
    find_DominantPosition(all_nums_forAd)
    find_DominantPosition(all_vanityNum_forAd)

    # Find the final_url, number, vanity_number based on the score.
    final_url, best_url_frame,url_score = find_MaxScoreItem(all_urls_forAd)
    # previous_ad_url = final_url
    #print('\n\t\t{}'.format(all_urls_forAd))
    # print('\t\t',all_urls_forAd)

    final_number, best_number_frame,number_score = find_MaxScoreItem(all_nums_forAd)
    # previous_ad_num = final_number
    # print('\n\t\t{}'.format(final_number) )
    # print('\t\t',all_nums_forAd)

    final_vanityNumber, best_vanityNumber_frame,vanityNumber_score = find_MaxScoreItem(all_vanityNum_forAd)
    # previous_ad_vanity = final_vanityNumber
    # print('\n\t\t{}'.format(final_vanityNumber) )

    # If download was present in atleast 1 frame then put yes.Otherwise, Put no.
    if downloadableFlag == 1:
        downloadable = 'yes'
    else:
        downloadable = 'no'
    '''
      If we have atleast one frame to process in this ad. 
          details : details key present in json file.
          Append channel_url, channel_telephone, channel_vanity, downlaodable to the details.
    '''
    overall_score=0#change this
    #overall_score=get_score()
    details_to_return['downloadable']=downloadable
    details_to_return['urls_data']={"url":final_url,"best_frame":best_url_frame,"confidence":url_score}
    details_to_return['numbers_data']={"number":final_number,"best_frame":best_number_frame,"confidence":number_score}
    details_to_return['vanityNumbers_data']={"vanityNumbers":final_vanityNumber,"best_frame":best_vanityNumber_frame,"confidence":vanityNumber_score}
    details_to_return['Text_In_Centre']=in_centre
    details_to_return['is_small']=is_small
    overall_score = get_score(details_to_return)
    details_to_return['overall_score']=overall_score

    # details_to_return.append(details)
    """df = pd.DataFrame( details_to_return,columns= ['channel_name', 'chan_start','ref_duration', 'rid', 'cnt', 'q_start','q_end','valid','downloadable','channel_url','channel_number','channel_vanity', 'bad_url/phone'] )
    df.to_csv('CENT-10292020-114500-1.csv', index=False)"""
    return details_to_return


def write_tsv(final_dictionary,file_out='/home/testuser/AD-Detect_Results/text/text_report.csv',debug=False):
  if not file_out:
    file_out ='text_report.tsv'
  #writing into tsv file row by row
  with open(file_out, 'wt') as out_file:
    try:
      tsv_writer = csv.writer(out_file, delimiter=',')
      if debug:
        header = ["Channel (qid)","Is Channel", "chan_start", "ref_duration", "Matching Ad ID", "cnt", "query_start", "query_end", "valid","telephone","vanity","url","downloadable","Text_in_centre","is_small","Overall_score"]
        tsv_writer.writerow(header)
        for query_result in final_dictionary.keys():
          row=final_dictionary[query_result]
          details=row['details']
          try:
            #for channel
            channel_result=row['r_0']
            result = main(channel_result)
            tsv_writer.writerow([details[0],True,details[1], details[2],details[3],details[4], details[5], details[6],details[7],result["numbers_data"],result["vanityNumbers_data"],result["urls_data"],result["downloadable"],result['Text_In_Centre'],result['is_small'],result["overall_score"]])
          except:
            traceback.print_exc()
            break
      else:
          header = ["Channel (qid)", "Is Channel", "chan_start", "ref_duration", "Matching Ad ID", "cnt", "query_start","query_end", "valid", "telephone", "vanity", "url", "downloadable","Overall_score"]
          tsv_writer.writerow(header)
          for query_result in final_dictionary.keys():
              row = final_dictionary[query_result]
              details = row['details']
              try:
                  channel_result = row['r_0']
                  result = main(channel_result)
                  tsv_writer.writerow([details[0], True, details[1], details[2], details[3], details[4], details[5],details[6], details[7], result["numbers_data"]["number"], result["vanityNumbers_data"]["vanityNumbers"],result["urls_data"]["url"], result["downloadable"],result["overall_score"]])
              except:
                  traceback.print_exc()
                  break
    except:
      traceback.print_exc()

#write_tsv(final_dictionary,file_out='/home/testuser/AD-Detect_Results/text/text_report.csv',debug=False)

def write_ad_tsv(final_dictionary,file_out=os.getcwd()+'/text_report.csv'):
    if not file_out:
        file_out ='text_report.tsv'
    #writing into tsv file row by row
    with open(file_out, 'wt') as out_file:
        try:
            tsv_writer = csv.writer(out_file, delimiter=',')
            header = ["rid(ad id)","telephone","vanity","url","downloadable","Text_in_centre","is_small","Overall_score","Processed_FrameTimeStamps"]
            tsv_writer.writerow(header)
            for query_result in final_dictionary.keys():
                Processed_FrameTimeStamps=[]
                row=final_dictionary[query_result]
                for i in row.keys():
                    Processed_FrameTimeStamps.append(row[i]['timestamp'])
                result = main(row)
                tsv_writer.writerow([query_result,result["numbers_data"],result["vanityNumbers_data"],result["urls_data"],result["downloadable"],result['Text_In_Centre'],result['is_small'],result["overall_score"],Processed_FrameTimeStamps])
        except:
            traceback.print_exc()
    return "Done"

