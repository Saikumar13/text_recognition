# Text Recognition 

## _SetUp Process_
### To Setup API

1. clone this repo
2. install PIP 
3. Change the current working dir to text_recognition 
4. create a virtual environment and activate it 
5. install required libraries using 
     `pip install -r requirements.txt`
6. start the API using gunicorn 
  `gunicorn -w 41 --reload --bind 0.0.0.0:8000 --capture-output --error-logfile error_log.txt --access-logfile log.txt -k uvicorn.workers.UvicornWorker -t 0 main:app
`
7. you can know more about api and test it by going to server_address:8000/docs 
8. you can hit the api with curl 
` curl -X POST "http://localhost:8000/text" -H  "accept: application/json" -H  "Content-Type: multipart/form-data" -F "files=@99612324.jpeg;type=image/jpeg"
`
*Note:* log is stored in error_log.txt
### To Use as function 

1. Create a virtual environment (optional)
2. Activate the virtual environment(optional)
3. install required libraries using pip install -r requirements.txt
4. Change the working directory to text_recoginiton
	`os.chdir('/content/text_recognition')`
5. import the function and call the functions to get the results
  `from main_v1 import GenTextOutput 
  GenTextOutput('/content/extracted_frames')`GenTextOutput('/content/extracted_frames')`
  
#### Note:
   __ code is taken from this repo [https://github.com/clovaai/deep-text-recognition-benchmark]  and 
   some modifications are made for our use case (done by silesh) (edited_by: sai) __
   
** Similar Files are merged under files with suffix '_utils'.Names of files that merged in them are mentioned at the top these files **

 Files 
 
   1. Train.py
   
   2. create_Imdb_dataset.py
   
are used for training the model

Note:

  1. To use the GPU call the `GenTextOutput('/content/extracted_frames',use_gpu=True)` use use_gpu=True
  2. To test with different pre-trained models use `GenTextOutput('/content/extracted_frames',saved_model='/pretrained_model/None-VGG-None-CTC.pth',Transformation='None',FeatureExtraction='VGG',SequenceModeling='None',Prediction='CTC',sensitive=False)`
   the parameterers(Transformation,...) values are in the  name of the model
   
   for example for model : 'None-VGG-None-CTC.pth',Transformation='None',FeatureExtraction='VGG',SequenceModeling='None',Prediction='CTC'
   
   For a case-sensitive model use sensitive=True