#!/bin/bash
#NOTIFYEMAIL=sai@streamn.ai

cmd=`lsof -n -i :8081 | wc -l`
if [ "$cmd" -eq "0" ];
then
	echo "API is Down."
	#if you want to sent the mail about server status Use your favorite mailer here:
        #mailx -s "Server $SERVERIP is down" -t "$NOTIFYEMAIL" < /dev/null
else
	echo "API service is running on port 8081"
fi
