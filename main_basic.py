from libraries import *

from model_utils import CRAFT, copyStateDict, RefineNet
from prediction_utils import test_net, demo
from file_utils import extract_frames, get_frames_list, loadImage, generate_words,get_FrameNumbers_FromTimeStamp,load_Frame_By_FrameNo_FromVideo,extract_frames_new,get_frames_list_new,loadImageFromFile
from regex import *
import uvicorn
from fastapi import FastAPI, File, UploadFile,Form, Request,Body,Form
from fastapi.middleware.cors import CORSMiddleware
from typing import List,Optional
from pydantic import BaseModel
from models import load_models
#solution to too many open files issue
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')

from models_basic import load_models
# initialization

mdl=load_models()
net,refine_net = mdl.craft(use_gpu=False)
model, device, converter = mdl.recognition(use_gpu=False,basic=True)
#model for regex(spacy model)
nlp, phn_matcher,vanity_matcher=mdl.load_regex_model(basic=True)

app = FastAPI()

#adding CORS(for allowing to hit from different addresses)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

"""
#to get dict as input
class p_args(BaseModel):
    basic: Optional[bool]=None
    use_gpu: Optional[bool]=None
    text_threshold:Optional[int]=None
    link_threshold:Optional[int]=None
    workers: Optional[int]=None
    refine: Optional[bool]=None
"""

# start of main function
#project_path = os.getcwd() + '/'


# project_path = os.getcwd()+'/text_recognition/'

@app.post("/text")
#async def create_files(files: Optional[List[UploadFile]] = File(None),is_video:bool=False,video_path:str=None,frame_timestamps:List[int]=[]):
def get_text(files: Optional[List[UploadFile]] = File(None),video_path:str=None,frame_timestamps:List[int]=[]):
#def GenTextOutput(process_file_path, frame_timestamps=[], frame_number=60, **passed_kwargs):
    '''
    arguments:
        files: frames passed as bytes
        video_path: folder path of frames or video path to extract frames
        frame_timestamps: timestamps of frames which needs to be extracte from the video (video_path)

    explanation:
        if video is passed in files:
                                if frame timestamps is passed runs text on those frames of video
                                else extracts the frames for every second and runs text on them
        if frames are passed then if text is run on the passed frames
        if files is empty , then if video_path is
                                    1.directory,then it should have frames inside the directory
                                    2.path to a video:if frame_timestamps
                                                            1.extract frames from the video
                                                            2.extract frame for every second

    returns: a json with text results
'''
    try:

        # --------------------- Reading the Input data ------------------------------
        is_video=False
        # checking if its a video or set of frames
        #if it is frames
        if files:
            process_file_path = files
            image_list = []
            images_names = []
            frame_timestamp = []
            for fle in files:
                #check if its a video file
                if '.m' in fle.filename:
                    if frame_timestamps:
                        # copy video file
                        destination = project_path + 'temp.mp4'
                        try:
                            with open(destination, "wb") as buffer:
                                shutil.copyfileobj(files[0].file, buffer)
                            file_name = fle.filename.split('.')[0]
                        finally:
                            fle.file.close()
                            # extract
                        image_list = get_FrameNumbers_FromTimeStamp(destination, frame_timestamps)
                        is_video = True
                    else:
                        image_list,frame_timestamps=extract_frames_new(file_path=video_path, frame_timestamps=[], frame_number=30, project_path=os.getcwd() + '/')
                else:
                    images_names.append(fle.filename)
                    image_list.append(loadImageFromFile(fle.file))
                    # shutil.copyfileobj(fle.file, buffer)
                    frame_timestamp.append(fle.filename.split('_')[-1].split('.')[0])
            if frame_timestamp:
                frame_timestamps=frame_timestamp
        #if frames are not passed chech if its frames folder or a video path
        else:
            if os.path.isdir(video_path):#if its a directory,it is supposed to have frames
                # read frames in the directory
                image_list, frame_timestamps = get_frames_list_new(video_path)
                file_name = False
            #if its a path to the video file
            elif os.path.isfile(video_path):
                if frame_timestamps:
                    is_video=True
                    #extract frames from the video and
                    image_list = get_FrameNumbers_FromTimeStamp(video_path, frame_timestamps)
                    destination=video_path
                    file_name = video_path.split('/')[-1][:-4]
                else:
                    #extract frames for evry second from the video
                    image_list,frame_timestamps=extract_frames(file_path=video_path, frame_timestamps=[], frame_number=30, project_path=os.getcwd() + '/')

                if not is_video:
                    if isinstance(image_list[0],str):
                        new_list=[]
                        for im in image_list:
                            new_list.append(loadImage(im))
                        image_list=new_list
            else:
                return "Please pass correct input parameters"


        #print("images names::", images_names)

        # --------------------- Processing the Arguments  ------------------------------

        # overwriting existing dict and appending new ones to final dict
        default_kwargs = {'trained_model': project_path + 'Pretrained_Models/craft_mlt_25k.pth',
                'basic':True,'use_gpu': False, 'refine': False, 'refiner_model': project_path+'Pretrained_Models/craft_refiner_CTW1500.pth',
                          'text_threshold': 0.7, 'low_text': 0.4, 'link_threshold': 0.4, 'canvas_size': 1280,
                          'mag_ratio': 1.5,'poly': False, 'show_time': False, 'test_folder': project_path + '/Frame_vid',
                          'image_folder': project_path + 'Cropped_image', 'workers': 0, 'batch_size': 1,
                          #'saved_model': project_path + 'Pretrained_Models/CRNN-PyTorchCTC.pth',
                          'saved_model': project_path + 'Pretrained_Models/None-VGG-None-CTC.pth',
                          'batch_max_length': 25, 'imgH': 32, 'imgW': 100,
                          'character': '0123456789abcdefghijklmnopqrstuvwxyz','sensitive': False, 'rgb': False, 'PAD': False,
#'Transformation': 'None','FeatureExtraction': 'CRNN','SequenceModeling': 'None', 'Prediction': 'CTC',
                          'Transformation': 'None','FeatureExtraction': 'VGG','SequenceModeling': 'None', 'Prediction': 'CTC',
                          'num_fiducial': 20, 'input_channel': 1,'output_channel': 512,'hidden_size': 256, 'debug': False
                          }
        kwargs = default_kwargs.copy()
        #kwargs.update(passed_kwargs.items())

    except Exception as er:
        print("error occured while reading the input data and/or arguments")
        print("error is ", er)
        sys.exit(1)
        # --------------------- Loading the model ------------------------------
    #  loading the model in main and passing it as parameter instead of loading it for every frame
        # --------------------- Predicting the Text and storing the output to Json ------------------------------
    try:
        t = time.time()
        # writing output to a file
        final_json = {}
        # for each image
        #for k, image_path in enumerate(image_list):
        #print(image_list)
        for k, actualFrameNumber_From_TimeStamp in enumerate(image_list):
            Json_format = {}
            #image = loadImage(image_path)
            if is_video:
                image = load_Frame_By_FrameNo_FromVideo(destination, actualFrameNumber_From_TimeStamp)
            else:
                image=image_list[k]
            #image_name = images_names[k].split('.')[0]#removing extension

            # --------------------- Detecting the Text Co-ordinates ------------------------------

            # returns the co-ordinates of the text detected in the images and also scores
            bboxes, polys, score_text, det_scores = test_net(net, image, kwargs['text_threshold'],
                                                             kwargs['link_threshold'],
                                                             kwargs['low_text'], kwargs['use_gpu'], kwargs['poly'],
                                                             kwargs, refine_net)

            """
            bboxes= the Bounding box 4-vertices coordinates(x,y)(w,h)
            polys= array of result file
            score_text= heatmap score used for mask image
            det_scores= confidence score for each bounding box
            """

            # variables used to store the output data
            full_text = {}
            Final_output = {}
            # fullTextAnnotation = []
            Json_format = {}
            block_dict = {}
            block_list = []
            # bbox_score={}       # Bbox_score dictionary for datatframe
            Word_list = []  # Word_list dictionary for all bounding boxes vertices
            Pred_txt = ''  # initialization of concat_text
            order_dict={} #for ordering based on co-ordinates
            cnt=0
            # for each detected box(word/words)
            for box_num in range(len(bboxes)):
                bboxes_json = {}  # for the return dictionary from bbox_cord file
                item = bboxes[box_num]  # 2-d array containing x & y coordinate

                # --------------------- genearating the images of each box using Co-ordinates  ------------------------------
                """ Crop_Evaluation file """
                # a image with the  co-ordinates in bboxes is generated in path passed to  "image_folder" parameter

                box_dim = generate_words(k, item, image, img_folder=kwargs['image_folder'], box_number=box_num,
                                         debug=kwargs['debug'])
                if len(box_dim)==4:
                    bboxes_json = {"X": box_dim[0], "Y": box_dim[1], "W": box_dim[2], "H": box_dim[3]}
                else:
                    bboxes_json = {"X":0, "Y":0, "W":0, "H":0}

                # --------------------- Predicting the text in the cropped images  ------------------------------

                """ Demo file """

                # calling demo function from demo.py file for getting predicted text on cropped images in image_folder
                pred, confidence_score = demo(model, device, converter,kwargs)  # return of predicted txt and confidence score
                Pred_txt += " " + pred  # concatenation of all predicted word

                # --------------------- Storing the data and writing to Json file   ------------------------------

                vertices_dict = {}  # initializing a empty dict

                vertices_dict["boundingBox"] = bboxes_json
                vertices_dict["confidence"] = confidence_score
                vertices_dict["text"] = pred.replace('\'','').replace('\"','')
                if str(pred) in order_dict.keys():
                  order_dict[str(pred)+"_"+str(cnt)] = [bboxes_json['X'],bboxes_json['Y'],bboxes_json['W'],bboxes_json['H']]
                  cnt=cnt+1
                else:
                  order_dict[str(pred)] = [bboxes_json['X'],bboxes_json['Y'],bboxes_json['W'],bboxes_json['H']]

                # for each word(bounding box) vertices,confidence score and text are appended into Word_list
                Word_list.append(vertices_dict)

            #ordering the text based on-cordinates
            Pred_txt=sort_dict(order_dict)
            #extracting urls and numbers
            nums,urls,text,downloadable,vanity_nums = extract_info(Pred_txt, kwargs['basic'], nlp, phn_matcher, vanity_matcher)
            #nums,urls,text,downloadable,vanity_nums = extract_info(Pred_txt,kwargs['basic'])
            #storing into dict
            Final_output["ConcatText"] = Pred_txt.replace('\'','').replace('\"','')
            Final_output["fullTextAnnotation"] = Word_list
            Json_format["frame_num"] = k
            if frame_timestamps:
                if isinstance(frame_timestamps[k],str):
                    if frame_timestamps[k].isdigit():
                        Json_format["timestamp"] = int(frame_timestamps[k])
                    else:
                        Json_format["timestamp"] = 999
                else:
                    Json_format["timestamp"] = frame_timestamps[k]
            else:
                Json_format["timestamp"] = 999

            Json_format['urls']=urls
            Json_format['nums']=nums
            Json_format['vanity_numbers']=vanity_nums
            Json_format['text']=text
            Json_format['downloadable']=downloadable
            Json_format["predicted_text"] = Final_output

            final_json[str(k)] = Json_format
            #Delete the previous image
            prev_img=default_kwargs['image_folder']+'/'+'image_o.jpg'
            if os.path.isfile(prev_img):
                os.remove(prev_img)
            #print(Json_format)
        # writing to Json file
        if kwargs['debug']:
            if file_name:
                out_pth = str(file_name) + '_results.json'
            else:
                out_pth = str(image_name) + '_results.json'
            with open(out_pth, 'w') as f:
                json.dump(final_json, f, indent=4)
        print("elapsed time : {}s".format(time.time() - t))

        return final_json
    except IndexError as er:
        print("error occured while Recognising the text")
        print("error is ", er)
        sys.exit(1)
    finally:
        frames_path=os.getcwd() + '/generated_frames/'
        destination=project_path + 'temp.mp4'
        if os.path.isdir(frames_path):
            shutil.rmtree(frames_path)
        #check for temp.mp4 file
        if os.path.isfile(destination):
            os.remove(destination)
        #stored image
        prev_img=default_kwargs['image_folder']+'/'+'image_o.jpg'
        if os.path.isfile(prev_img):
            os.remove(prev_img)

#gunicorn --workers=5 --threads=3 --reload --bind 0.0.0.0:8000 --capture-output --error-logfile error_log.txt --access-logfile log.txt -k uvicorn.workers.UvicornWorker -t 0 main:app
#pkill gunicorn for killing the gunicorn process
if __name__ == "__main__":
    uvicorn.run('main:app', host='0.0.0.0', port=8080)
