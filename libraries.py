import sys
import os
import time
import re
import shutil
import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
from PIL import Image
import cv2
import mmcv
from skimage import io
import numpy as np
import zipfile
import pandas as pd
import shutil
from collections import OrderedDict
#file_utils libraries
import logging
import traceback
#from google.colab.patches import cv2_imshow
import six
import math
#import lmdb
from natsort import natsorted
from torch.utils.data import Dataset, ConcatDataset, Subset
from torch._utils import _accumulate
import torchvision.transforms as transforms
#model_utils libraries
from collections import namedtuple
import torch.nn.init as init
from torchvision import models
from torchvision.models.vgg import model_urls
import torch.nn.functional as F
from modules.transformation import TPS_SpatialTransformerNetwork
from modules.feature_extraction import VGG_FeatureExtractor, RCNN_FeatureExtractor, ResNet_FeatureExtractor
from modules.sequence_modeling import BidirectionalLSTM
from modules.prediction import Attention
#prediction_utils

#craft_utils
import json
import string
import torch.utils.data

#solution to too many open files issue
import torch.multiprocessing
torch.multiprocessing.set_sharing_strategy('file_system')
project_path='/home/streamn/TextRecognition/text_recognition/'
#project_path = os.getcwd() + '/'
