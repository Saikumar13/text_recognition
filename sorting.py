#imports 

import collections
import re

#function to remove extra spaces and /n 's

def removeNL(finalstring:str):
    l = re.sub(' +', ' ', finalstring)
    l = l.split("/n")
    return '/n'.join([x for x in l if len(x)>1])

''' sai's code '''


def CheckIfPresent(same_block,output_array):
    match=False
    #check if the block is already appended 
    for blk  in range(len(output_array)):
        for bl in  output_array[blk]:
            if bl in same_block:
                #check if the already appended one length is more than current
                if len(same_block)>len(output_array[blk]):
                    output_array.pop(blk)
                    output_array.append(sorted(same_block, key=lambda k: [k[1],k[0]]))
                match=True
    return match
    #return same_block,output_array,match

def IsLessConfidenceText(text,text_confidence):

    #checks if the confidence is less than required

    #0.35 for 3 letters going down to 0.15 for > 8 letters
    need_to_remove=True
    if len(str(text))<=3:
        conf_threshold=0.3
    elif 3<len(str(text))<8:
        conf_threshold=0.2
    else:
        conf_threshold=0.1
    #print('Confidence: ',value['confidence'])
    if float(text_confidence)>float(conf_threshold):
        need_to_remove=False
    else:
        pass
    return need_to_remove

def SortProperly(dt,inpt_array=[],debug=True):

    #sort list or dictonary based on y then x values if y value is less than ewual to 2 it is considered to be of same line 
    if dt:
        sorted_list = sorted(dt, key=lambda k: [dt[k]['co-ords'][1], dt[k]['co-ords'][0]], reverse=False)
        srt_dict = collections.OrderedDict({k: dt[k] for k in sorted_list})
        words_coords = [[key, dt[key] ]for key in  [x for x in dt] ]
        input_array = list(v['co-ords'] for v in srt_dict.values())
    else:
        input_array=inpt_array
    out_array=[]
    mapped_words=[]
    for i in range(0,len(input_array)):
        same_row=[]
        reference_pos=input_array[i]
        if reference_pos in mapped_words:
            continue
        same_row.append(reference_pos)
        reference_y=reference_pos[1]
        reference_x=reference_pos[0]
        for j in range(i+1,len(input_array)):
            query_pos=input_array[j]
            query_y=query_pos[1]
            query_x=query_pos[0]

            if query_pos in mapped_words:
                continue

            if debug:
                pass
                #print("refernce {} \t query {}".format(reference_pos,query_pos))
                #print("refernce y {} \t query y{}".format(reference_y,query_y))
            if reference_pos[3]==0:
                y_sim_dist = (query_pos[1] - reference_pos[1] ) / 1
            else:
                y_sim_dist = (query_pos[1] - reference_pos[1] ) / reference_pos[3]


            #if reference_y-2<=query_y<=reference_y+2:
            if y_sim_dist<0.3:
                if query_pos not in same_row:
                    same_row.append(query_pos)
                    mapped_words.append(query_pos)
            else:
                break
        out_array.append(sorted(same_row,key=lambda k:k[0],reverse=False))
    o_array=[]
    for i in out_array:
        for j in i:
            o_array.append(j)
    if debug:
        print(o_array)
    return o_array

#def CheckSameBlockandAppend(in_pos,input_array,words_coords,debug=False):
def CheckSameBlockandAppend(in_pos,input_array,words_coords,same_block,output_array,mapped_bouding_boxes,same_block_text,debug=False):
    #check if the remaining words belong to same block 
    reference_bounding_box=input_array[in_pos]
    for pos3 in range(in_pos+1,len(input_array)):

        query_bounding_box=input_array[pos3]
        reference_word = [x[0] for x in words_coords if x[1]['co-ords'] == reference_bounding_box]
        query_word = [x[0] for x in words_coords if x[1]['co-ords'] == query_bounding_box]
        numberofletters = len(reference_word)
        if numberofletters==0:
            continue

        letter_width=reference_bounding_box[2]/numberofletters 

        
        #calculate x_sim_dist
        dst1=abs((reference_bounding_box[0] + reference_bounding_box[2])-(query_bounding_box[0]+query_bounding_box[2]))
        dst2=abs((reference_bounding_box[0])-(query_bounding_box[0]+query_bounding_box[2]))
        dst3=abs((reference_bounding_box[0] + reference_bounding_box[2])-(query_bounding_box[0]))
        dst4=abs((reference_bounding_box[0])-(query_bounding_box[0]))
        min_dist=min(dst1,dst2,dst3,dst4)



        if letter_width==0:
            x_sim_dist = min_dist / 1
        else:
            x_sim_dist = min_dist / letter_width 

        if reference_bounding_box[3]==0:
            y_sim_dist = (query_bounding_box[1] - reference_bounding_box[1] ) / 1
        else:
            y_sim_dist = (query_bounding_box[1] - reference_bounding_box[1] ) / reference_bounding_box[3]

        if max(query_bounding_box[3], query_bounding_box[3] )==0:
            h_sim_dist = 2 * abs(query_bounding_box[3] - reference_bounding_box[3])/ 1
        else:
            h_sim_dist = 2 * abs(query_bounding_box[3] - reference_bounding_box[3])/max(query_bounding_box[3], query_bounding_box[3] )

        if min_dist > 5 or h_sim_dist>3:    #
            break

        if debug:
            print("x_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,x_sim_dist))
            print("y_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,y_sim_dist))
            print("h_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,h_sim_dist))

        if y_sim_dist <= 1 and x_sim_dist<=0.57:

            #if y_sim_dist < 0.3: #same row
            if query_bounding_box not in same_block:
                same_block.append(query_bounding_box)
                if debug:
                    print("adding {} to block {}".format(query_word,same_block_text))
                same_block_text.append(query_word)

            if reference_bounding_box not in same_block:
                same_block.append(reference_bounding_box)
                if debug:
                    print("adding {} to block {}".format(reference_word,same_block_text))
                same_block_text.append(reference_word)

            mapped_bouding_boxes.append(query_bounding_box)
            CheckSameBlockandAppend(pos3,input_array,words_coords,same_block,output_array,mapped_bouding_boxes,same_block_text,debug=False)
        else:
            if mapped_bouding_boxes:
                mapped_bouding_boxes.pop(-1)
    match=CheckIfPresent(same_block,output_array)
    if not match:
        output_array.append(SortProperly(dt='',inpt_array=same_block,debug=debug))


def sort_dict_sai(dt,debug=True):
    finalText=""
    words_coords = [[key, dt[key] ]for key in  [x for x in dt]]
    # ordering by y &x if y values is +-2 it is considered to be same row
    input_array = SortProperly(dt=dt,inpt_array=[],debug=debug)
    if debug:
        print("input_array: {}".format(input_array))
    output_array=[]
    mapped_bouding_boxes=[]
    same_block=[]

    # 1.go through each bounding box 
    # 2.check the x and y Dist and 
    # 3.append to a block or create a new block
    for pos in range(0, len(input_array)):
        reference_bounding_box = input_array[pos]
        #if the bouding box is already append to any block skip the bounding box or word
        if reference_bounding_box in mapped_bouding_boxes:
            continue
        else:
            mapped_bouding_boxes.append(reference_bounding_box)
        reference_word = [x[0] for x in words_coords if x[1]['co-ords'] == reference_bounding_box]
        reference_word_confidence_list=[v['confidence'] for v in dt.values() if v['co-ords'] == reference_bounding_box]
        reference_word_confidence=0
        if debug:
            print("Processing word{} ".format(reference_word))

        if len(reference_word_confidence_list):
            reference_word_confidence= reference_word_confidence_list[0]
        numberofletters = len(reference_word)
        #if the text is empty or if the confidence is less, discard the Text(box)
        #if numberofletters == 0 or IsLessConfidenceText(text=reference_word,text_confidence=reference_word_confidence)==True:
        if numberofletters==0:
            continue
        
        letter_width=reference_bounding_box[2]/numberofletters 
        same_block=[]
        same_block_text=[]

        if reference_bounding_box not in same_block:
            same_block.append(reference_bounding_box)
            same_block_text.append(reference_word)

        ''' Find the distance of other words from the current word and decide to add in same block '''

        #walk though remaining  list till next row if it doesnt fall in same block (> 1.5 * H is reached)
        for pos2 in range(pos+1, len(input_array)):           
            query_bounding_box = input_array[pos2]
            
            numberofletters = len(reference_word)
            if numberofletters>0:
                letter_width=reference_bounding_box[2]/numberofletters 
            else:
                continue
            
            #if the bouding box is already append to block
            if query_bounding_box in mapped_bouding_boxes:
                continue
            
            query_word = [x[0] for x in words_coords if x[1]['co-ords'] == query_bounding_box]
            
            ''' calculating distances from refernce word to query word '''
            
            #calculate x_sim_dist , min Distance from all four sides (since the word  can like just beneath the current word)
            #or sometimes it can be greater the length of first word to cover all cases taking min from all sides
            dst1=abs((reference_bounding_box[0] + reference_bounding_box[2])-(query_bounding_box[0]+query_bounding_box[2]))
            dst2=abs((reference_bounding_box[0])-(query_bounding_box[0]+query_bounding_box[2]))
            dst3=abs((reference_bounding_box[0] + reference_bounding_box[2])-(query_bounding_box[0]))
            dst4=abs((reference_bounding_box[0])-(query_bounding_box[0]))
            min_dist=min(dst1,dst2,dst3,dst4)
            
            if letter_width==0:
                x_sim_dist = min_dist / 1
            else:
                x_sim_dist = min_dist / letter_width 

            if reference_bounding_box[3]==0:
                y_sim_dist = (query_bounding_box[1] - reference_bounding_box[1] ) / 1
            else:
                y_sim_dist = (query_bounding_box[1] - reference_bounding_box[1] ) / reference_bounding_box[3]

            if max(query_bounding_box[3], query_bounding_box[3] )==0:
                h_sim_dist = 2 * abs(query_bounding_box[3] - reference_bounding_box[3])/ 1
            else:
                h_sim_dist = 2 * abs(query_bounding_box[3] - reference_bounding_box[3])/max(query_bounding_box[3], query_bounding_box[3] )

            if debug:
                print("x_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,x_sim_dist))
                print("y_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,y_sim_dist))
                print("h_sim_dist of word {} from word {} is: {}".format(query_word,reference_word,h_sim_dist))

            if y_sim_dist <= 1 and x_sim_dist<=0.57:
            #if h_sim_dist < 1 and x_sim_dist<1 and y_sim_dist<1:
                #if y_sim_dist < 0.3: #same row
                if query_bounding_box not in same_block:
                    same_block.append(query_bounding_box)
                    if debug:
                        print("adding {} to block {}".format(query_word,same_block_text))
                    same_block_text.append(query_word)

                mapped_bouding_boxes.append(query_bounding_box)
                if debug:
                    print('output_array before recursive call: {}'.format(output_array))
                
                #search the other words that are near to the word
                CheckSameBlockandAppend(pos2,input_array,words_coords,same_block,output_array,mapped_bouding_boxes,same_block_text,debug=debug)
                
                if debug:
                    print('output_array after recursive call: {}'.format(output_array))

            else:
                pass
            
        match=CheckIfPresent(same_block,output_array)#checks if the elemnts in the list are already present if present check length and append biggest one else append directly 
        if not match:
            output_array.append(SortProperly(dt='',inpt_array=same_block,debug=debug))

    sorted_block=SortProperly(dt='',inpt_array=same_block,debug=debug)

    match=CheckIfPresent(sorted_block,output_array)
    if not match:
        output_array.append(sorted_block)
    if debug:
        print('output_array at the end: {}'.format(output_array))
    srt_list = []
    #get text corresponding to  the pos and append the Text
    for row in output_array:
        if row:
            for i in row:
                #print("i",i)
                for key, value in dt.items():
                    #print(value)
                    if value['co-ords'] == i:
                        #print(i,key)
                        #0.35 for 3 letters going down to 0.15 for > 8 letters
                        if len(str(key))<=3:
                            conf_threshold=0.3
                        elif 3<len(str(key))<8:
                            conf_threshold=0.2
                        else:
                            conf_threshold=0.1
                        #print('Confidence: ',value['confidence'])
                        if isinstance(value['confidence'],dict):
                            value['confidence']=0.5
                        else:
                            pass
                        if float(value['confidence'])>float(conf_threshold):
                            srt_list.append(key)
                            finalText=finalText+" "+key.split('_')[0]
                        else:
                            pass
            finalText=finalText+'/n'
    finalText = removeNL(finalText)
    return finalText






'''   Joe's Code   '''


def sort_array(dt, debug=0):
    height_thresh = 1.2
    # ordering by y &x
    sorted_list = sorted(dt, key=lambda k: [dt[k]['co-ords'][1], dt[k]['co-ords'][0]], reverse=False)
     
    srt_dict = collections.OrderedDict({k: dt[k] for k in sorted_list})
    #print("Srt dict: ",srt_dict)
    input_array = list(v['co-ords'] for v in srt_dict.values())
    sorted_array = []
    already_joined = []

    words_coords = [[key, dt[key] ]for key in  [x for x in dt]  ]

    for pos in range(0, len(input_array)):
        if pos in already_joined:
            #print ("pos {} already joined".format(pos))
            continue
        ref_block = input_array[pos]
        local_array = [ref_block]
        already_joined.append(pos)
        word = [x[0] for x in words_coords if x[1]['co-ords'] == ref_block]
        if debug == 1: print ("pos {} word {}".format(pos, word))
        if len(word)>0:
            cur_word = word[0]
        else:
            continue    
        #print("word", cur_word)
        num_letters = len(cur_word)
        letter_width=ref_block[2]/num_letters
        if num_letters == 0:
            continue

        for pos2 in range(pos+1, len(input_array)):
            if pos2 in already_joined:
                #print ("pos {}, pos2 {} pos2 already joined".format(pos, pos2))
                continue
            test_block = input_array[pos2]
            word2 = [x[0] for x in words_coords if x[1]['co-ords'] == test_block]       
            if debug == 1: print ("pos2 {} word {}".format(pos2, word2))                 
            #join if height diff is less than 70% and x diff < 1 x letter width; together < 1.5
            if debug == 1: print ("test_block {} ref_block {}")
            x_dist = test_block[0] - (ref_block[0] + ref_block[2])
            #if debug == 1: print ("if test >  x_dist {}".format(x_dist))
            if ref_block[0] > test_block[0]:
                x_dist = ref_block[0] - (test_block[0] + test_block[2])
                #if debug == 1: print ("if test <  x_dist {}".format(x_dist))
            #if x_dist < 0:
            #    x_dist = 300
            y_dist = abs(test_block[1] - ref_block[1] )   / max(test_block[3], ref_block[3])

            if y_dist > height_thresh:
                break
            if debug==1: 
                print("pos {} , pos2 {} x_dist {}, letter_width {} y_dist {}".format(pos, pos2, x_dist, letter_width, y_dist))
            if y_dist < 0.5 and x_dist / letter_width < 0.9:
                already_joined.append(pos2)
                local_array.append(test_block)

        if len(local_array) > 0:
            #print("local_array", local_array)
            sorted_local = sorted(local_array, key=lambda x : x[0])
            #print("sorted_local", sorted_local)
            for block in sorted_local:
                sorted_array.append(block)
        #print("already_joined", already_joined)

        #print("sorted_array", sorted_array)
    return sorted_array             

import collections
def sort_dict_joe(dt, debug = 0):
    finalText=""
    # ordering by y &x
    #sorted_list = sorted(dt, key=lambda k: [dt[k]['co-ords'][1], dt[k]['co-ords'][0]], reverse=False)
    
    srt_dict = collections.OrderedDict({k: dt[k] for k in dt})
    #print("Srt dict: ",srt_dict)
    #sort array using row logic in sort_array
    #input_array =  list(v['co-ords'] for v in srt_dict.values()) 
    input_array = sort_array(dt)

    if debug == 1: print("in", input_array)
    output_array = []
    previous_row = []
    already_joined = []
    height_thresh = 1.8 #please calculate it as required from actual channel specific data
    sim_thresh = 0.9
    
    #walk through output_array and sor
    
    #first iter with only x neighbors
    #re-sort elements of a row 
    iter=0
    words_coords = [[key, dt[key] ]for key in  [x for x in dt]  ]
    row_output = []
    while (iter < 2) :
        #create assocaited array to enable word recovery; needed for letter width clculation for x_sim_dist                  
    
        if debug == 1: print(" iter : {}, woord_coords:{} ".format(iter, words_coords) )
        output_array = []
        previous_row = []
        already_joined = []
        total_dist_x=100
        total_dist_y=100
        last_row_min = 1000
        last_row_max = 0
        last_row_ht_min = 1000
        last_row_ht_max = 0
        for pos in range(0, len(input_array)):
            if debug == 1: print("input box", input_array[pos])
            output_array.append([])
            #append empty array for each ref_block
            #append other blocks to each of the above blocks as we find best neighbor blocks
            #create empty array for every pos in input

            ref_block = input_array[pos]
            word = [x[0] for x in words_coords if x[1]['co-ords'] == ref_block]
            if len(word)>0:
                cur_word = word[0]
            else:
                continue    
            if debug == 1: print("word", cur_word)
            num_letters = len(cur_word)
            letter_width=ref_block[2]/num_letters
            if num_letters == 0:
                continue
            min_dist = 100
            min_pos = -1
            total_dist_x=100
            total_dist_y=100
            #walk though remaining  list till next row > 1.5 * H is reached
            for pos2 in range(pos+1, len(input_array)):           
                test_block = input_array[pos2]
                if test_block in [x[1] for x in already_joined]:
                    #create empty block
                    continue
                test_word = [x[0] for x in words_coords if x[1]['co-ords'] == test_block]
                try: 
                    x_sim_dist = abs(test_block[0] - (ref_block[0] + ref_block[2])) / letter_width
                except:
                    x_sim_dist = (test_block[0] - (ref_block[0] - ref_block[2])) / ref_block[3]
                y_sim_dist = (test_block[1] - ref_block[1] ) / ref_block[3]
                h_sim_dist = 2 * abs(test_block[3] - ref_block[3])/max(test_block[3], ref_block[3] )
                if debug == 1: print("x_sim_dist of word {} from word {} is: {}".format(test_word,cur_word,x_sim_dist))
                if debug == 1: print("y_sim_dist of word {} from word {} is: {}".format(test_word,cur_word,y_sim_dist))
                if debug == 1: print("h_sim_dist of word {} from word {} is: {}".format(test_word,cur_word,h_sim_dist))
                #separate distance measures for joining on x axis and y-axis

                if iter > 0:
                    if y_sim_dist > 0.75 and h_sim_dist < 0.7: 
                        #new row and other row elements are ordered hence expecting 2x letter_width
                        if test_block[0] < last_row_min :
                             last_row_min = test_block[0]
                        if test_block[0] + test_block[2] > last_row_max :
                             last_row_max = test_block[0] + test_block[2]      
                        if test_block[1] < last_row_ht_min :
                             last_row_ht_min = test_block[1]
                        if test_block[1] + test_block[3] > last_row_ht_max :
                             last_row_ht_max = test_block[1] + test_block[3]  

                    else:
                        last_row_min = 1000
                        last_row_max = 0
                        last_row_ht_min = 1000
                        last_row_ht_max = 0                              

                if y_sim_dist < 0.3:#check if its in the same row
                    total_dist_x = x_sim_dist + h_sim_dist
                    if h_sim_dist < 0.6 and x_sim_dist < 0.6:
                        total_dist_x = x_sim_dist + h_sim_dist/2
                #0.7 is used to enable 40% overlap; 30% on each side allowed to be not overlapping
                #checking for overlap
                if (ref_block[0] < test_block[0]+test_block[2]*0.7 and test_block[0] < ref_block[0]+ref_block[2]*0.7)\
                    and iter > 0:
                    #test of pos is part of a longer row; if part of longer row; join pos and min_pos if pos is last element of joined row
                    last_word_in_row = [x[-1] for x in row_output if x!=[]]
                    is_last = 1 if ref_block in last_word_in_row else 0
                    first_word_in_row = [x[0] for x in row_output if x!=[]]
                    is_first = 1 if test_block in first_word_in_row else 0
                    #adding letter width test to check if first element of a new row
                    if is_last == 1 and is_first == 1 and (test_block[0] - last_row_min < 2 * letter_width) :
                        total_dist_y = y_sim_dist -1 + h_sim_dist 
                total_dist = min(total_dist_x, total_dist_y)
                if total_dist < min_dist and total_dist < sim_thresh:
                    min_dist = total_dist
                    min_pos = pos2
                if debug == 1: 
                  print("pos {}\t, pos2 {}\t, min_dist: {}\t, min_pos {}\t Total_dist_y: {}\t total_dist_x {} \t total_dist: \
                  {}\t h_sim_dist : {} \t Height threshold * height: {}\t y_sim_dist: {}".format(pos, pos2, \
                  min_dist, min_pos, total_dist_y,total_dist_x,total_dist, h_sim_dist, height_thresh * ref_block[3], y_sim_dist))
                if y_sim_dist > height_thresh : #* ref_block[3] :
                    if debug == 1: 
                        if debug == 1: print("Inside append code, break total_y_dist {}\t ref height {}".format(total_dist_y, ref_block[3] ) )
                    if min_pos != -1 :
                        if debug == 1: print ("pos {}  already_joined {}".format(pos, already_joined))
                        if pos not in [x[1] for x in already_joined]:
                            #first 
                            if debug == 1: print("existing", output_array[pos])
                            if output_array[pos] == []:
                                output_array[pos].append(input_array[pos])
                            output_array[pos].append(input_array[min_pos])
                            if debug == 1: print("New bloc {}".format(output_array[pos]) )
                        else:
                            append_loc = [x[0] for x in already_joined if x[1]==pos]
                            if append_loc == []:
                                print ("error : empty append_loc")
                            if debug == 1: print("append_loc {}, current pos {}".format(append_loc[0], pos))
                            if debug == 1:print("out arr {}".format(output_array[append_loc[0]]))
                            output_array[append_loc[0]].append(input_array[min_pos])
                            if debug == 1:print("append to bloc {}".format(output_array[append_loc[0]]) )
                            
                        already_joined.append([pos, min_pos])
                    if debug == 1: print("at break {}".format(already_joined))
                    min_pos=-1
                    break
            if min_pos != -1:           
                #append input_array[min_pos] with input_array[pos]
                if pos not in [x[1] for x in already_joined]:
                    if output_array[pos] == []:
                        output_array[pos].append(input_array[pos])
                    output_array[pos].append(input_array[min_pos])
                    if debug == 1: print("End: New bloc {}".format(output_array[pos]) )
                else:
                    append_loc = [x[0] for x in already_joined if x[1]==pos]
                    if debug == 1: print("End: append_loc {}, current pos {}".format(append_loc[0], min_pos))
                    output_array[append_loc[0]].append(input_array[min_pos])
                    if debug == 1: print("End: append to bloc {}".format(output_array[append_loc[0]]) )
                already_joined.append([pos, min_pos])
                if debug == 1: print("at break {}".format(already_joined))
            else:
                if debug == 1: print("pos, out arr [pos]", output_array[pos])
                if pos not in [x[1] for x in already_joined] and output_array[pos] == []:
                    #append input_array[pos] for standalone block
                    output_array[pos].append(input_array[pos])
                    if debug == 1: print("Standalone bloc {}".format(output_array[pos]) )
            if debug == 1: print("Loop: output_array: {}".format(output_array))  
        #sort each group in output array by x position
        row_output = [ sorted(r) for r in output_array ]
        if debug == 1: print("output_array", output_array)
        if debug == 1: print ("row_output", row_output)
        #row_output = mod_rows
        input_array = [item for sublist in row_output for item in sublist]    

        words_iter = [] 
        if iter == 0:
            for row in row_output: #output_array:
                for i in row:
                    #print("i",i)
                    for key, value in srt_dict.items():
                        #print(value)
                        if value['co-ords'] == i:
                            words_iter.append([key, value])
            if debug == 1: print ("words after iter=0", words_iter)    
            if debug == 1: print("new input_array", input_array)  
            words_coords = words_iter           
        iter += 1  
        #dt = input_array  

    srt_list = []
    #print("output_array: {}".format(output_array))
    for row in output_array:
        if row:
            for i in row:
                #print("i",i)
                for key, value in srt_dict.items():
                    #print(value)
                    if value['co-ords'] == i:
                        #print(i,key)
                        #0.35 for 3 letters going down to 0.15 for > 8 letters
                        if len(str(key))<=3:
                            conf_threshold=0.3
                        elif 3<len(str(key))<8:
                            conf_threshold=0.2
                        else:
                            conf_threshold=0.1
                        #print('Confidence: ',value['confidence'])
                        if isinstance(value['confidence'],dict):
                            value['confidence']=0.5
                        else:
                            pass
                        if float(value['confidence'])>float(conf_threshold):
                            srt_list.append(key)
                            finalText=finalText+" "+key.split('_')[0]
                        else:
                            pass
            finalText=finalText+'/n'
    finalText = removeNL(finalText)
    return finalText
