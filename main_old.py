#used before 29/01/2021 (before adding extra functionaliteies like path and frame_timestamps)
# fastapi
from libraries import *

from model_utils import CRAFT, copyStateDict, RefineNet
from prediction_utils import test_net, demo
from file_utils import extract_frames, get_frames_list, loadImage, generate_words
from regex import *
import uvicorn
from fastapi import FastAPI, File, UploadFile,Form, Request,Body
from fastapi.middleware.cors import CORSMiddleware
from typing import List,Optional
from pydantic import BaseModel
from models import load_models
# initialization

mdl=load_models()
net,refine_net = mdl.craft(use_gpu=False)
model, device, converter = mdl.recognition(use_gpu=False,basic=False)
#model for regex(spacy model)
nlp, phn_matcher,vanity_matcher=mdl.load_regex_model(basic=False)

app = FastAPI()

#adding CORS(for allowing to hit from different addresses)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

"""
#to get dict as input
class p_args(BaseModel):
    basic: Optional[bool]=None
    use_gpu: Optional[bool]=None
    text_threshold:Optional[int]=None
    link_threshold:Optional[int]=None
    workers: Optional[int]=None
    refine: Optional[bool]=None
"""

# start of main function
project_path = os.getcwd() + '/'


# project_path = os.getcwd()+'/text_recognition/'
@app.post("/text")
def get_text(files: List[UploadFile] = File(...)):
#def GenTextOutput(process_file_path, frame_timestamps=[], frame_number=60, **passed_kwargs):
    try:
        # --------------------- Reading the Input data ------------------------------
        # checking if its a video or set of frames
        process_file_path=files
        if type(process_file_path) is list:
            image_list=[]
            images_names=[]
            frame_timestamps=[]
            for fle in files:
                images_names.append(fle.filename)
                image_list.append(loadImage(fle.file))
                    #shutil.copyfileobj(fle.file, buffer)
                frame_timestamps.append(fle.filename.split('_')[-1][:-5])
        elif os.path.isdir(process_file_path):
            image_list, frame_timestamps = get_frames_list(process_file_path)
            file_name = False
        else:
            return "Please pass correct input"

        print("images names::", images_names)

        # --------------------- Processing the Arguments  ------------------------------

        # overwriting existing dict and appending new ones to final dict
        default_kwargs = {'trained_model': project_path + 'Pretrained_Models/craft_mlt_25k.pth',
                          'basic':False,'use_gpu': False, 'refine': False, 'refiner_model': project_path+'Pretrained_Models/craft_refiner_CTW1500.pth',
                          'text_threshold': 0.7, 'low_text': 0.4, 'link_threshold': 0.4, 'canvas_size': 1280,
                          'mag_ratio': 1.5,'poly': False, 'show_time': False, 'test_folder': project_path + '/Frame_vid',
                          'image_folder': project_path + 'Cropped_image', 'workers': 4, 'batch_size': 192,
                          'saved_model': project_path + 'Pretrained_Models/TPS-ResNet-BiLSTM-Attn-case-sensitive.pth',
                          'batch_max_length': 25, 'imgH': 32, 'imgW': 100,
                          'character': '0123456789abcdefghijklmnopqrstuvwxyz','sensitive': True, 'rgb': False, 'PAD': False,
                          'Transformation': 'TPS','FeatureExtraction': 'ResNet','SequenceModeling': 'BiLSTM', 'Prediction': 'Attn',
                          'num_fiducial': 20, 'input_channel': 1,'output_channel': 512,'hidden_size': 256, 'debug': False
                          }
        kwargs = default_kwargs.copy()
        #kwargs.update(passed_kwargs.items())

    except Exception as er:
        print("error occured while reading the input data and/or arguments")
        print("error is ", er)
        sys.exit(1)
        # --------------------- Loading the model ------------------------------
    #  loading the model in main and passing it as parameter instead of loading it for every frame
        # --------------------- Predicting the Text and storing the output to Json ------------------------------
    try:
        t = time.time()
        # writing output to a file
        final_json = {}
        # for each image
        for k, image_path in enumerate(image_list):
            Json_format = {}
            #image = loadImage(image_path)
            #image = loadImage(image_path)
            image=image_path
            image_name = images_names[k].split('.')[0]#removing extension

            # --------------------- Detecting the Text Co-ordinates ------------------------------

            # returns the co-ordinates of the text detected in the images and also scores
            bboxes, polys, score_text, det_scores = test_net(net, image, kwargs['text_threshold'],
                                                             kwargs['link_threshold'],
                                                             kwargs['low_text'], kwargs['use_gpu'], kwargs['poly'],
                                                             kwargs, refine_net)

            """
            bboxes= the Bounding box 4-vertices coordinates(x,y)(w,h)
            polys= array of result file
            score_text= heatmap score used for mask image
            det_scores= confidence score for each bounding box
            """

            # variables used to store the output data
            full_text = {}
            Final_output = {}
            # fullTextAnnotation = []
            Json_format = {}
            block_dict = {}
            block_list = []
            # bbox_score={}       # Bbox_score dictionary for datatframe
            Word_list = []  # Word_list dictionary for all bounding boxes vertices
            Pred_txt = ''  # initialization of concat_text
            order_dict={} #for ordering based on co-ordinates
            cnt=0
            # for each detected box(word/words)
            for box_num in range(len(bboxes)):
                bboxes_json = {}  # for the return dictionary from bbox_cord file
                item = bboxes[box_num]  # 2-d array containing x & y coordinate

                # --------------------- genearating the images of each box using Co-ordinates  ------------------------------
                """ Crop_Evaluation file """
                # a image with the  co-ordinates in bboxes is generated in path passed to  "image_folder" parameter

                box_dim = generate_words(image_name, item, image, img_folder=kwargs['image_folder'], box_number=box_num,
                                         debug=kwargs['debug'])
                if len(box_dim)==4:
                    bboxes_json = {"X": box_dim[0], "Y": box_dim[1], "W": box_dim[2], "H": box_dim[3]}
                else:
                    bboxes_json = {"X":0, "Y":0, "W":0, "H":0}

                # --------------------- Predicting the text in the cropped images  ------------------------------

                """ Demo file """

                # calling demo function from demo.py file for getting predicted text on cropped images in image_folder
                pred, confidence_score = demo(model, device, converter,kwargs)  # return of predicted txt and confidence score
                Pred_txt += " " + pred  # concatenation of all predicted word

                # --------------------- Storing the data and writing to Json file   ------------------------------

                vertices_dict = {}  # initializing a empty dict

                vertices_dict["boundingBox"] = bboxes_json
                vertices_dict["confidence"] = confidence_score
                vertices_dict["text"] = pred.replace('\'','').replace('\"','')
                if str(pred) in order_dict.keys():
                  order_dict[str(pred)+"_"+str(cnt)] = [bboxes_json['X'],bboxes_json['Y'],bboxes_json['W'],bboxes_json['H']]
                  cnt=cnt+1
                else:
                  order_dict[str(pred)] = [bboxes_json['X'],bboxes_json['Y'],bboxes_json['W'],bboxes_json['H']]

                # for each word(bounding box) vertices,confidence score and text are appended into Word_list
                Word_list.append(vertices_dict)

            #ordering the text based on-cordinates
            Pred_txt=sort_dict(order_dict)
            #extracting urls and numbers
            nums,urls,text,downloadable,vanity_nums = extract_info(Pred_txt, kwargs['basic'], nlp, phn_matcher, vanity_matcher)
            #nums,urls,text,downloadable,vanity_nums = extract_info(Pred_txt,kwargs['basic'])
            #storing into dict
            Final_output["ConcatText"] = Pred_txt.replace('\'','').replace('\"','')
            Final_output["fullTextAnnotation"] = Word_list
            Json_format["frame_num"] = k
            if frame_timestamps:
                if frame_timestamps[k].isdigit():
                    Json_format["timestamp"] = int(frame_timestamps[k])
                else:
                    Json_format["timestamp"] = 999
            else:
                Json_format["timestamp"] = 999
            Json_format['urls']=urls
            Json_format['nums']=nums
            Json_format['vanity_numbers']=vanity_nums
            Json_format['text']=text
            Json_format['downloadable']=downloadable
            Json_format["predicted_text"] = Final_output

            final_json[str(k)] = Json_format
            print(Json_format)
        # writing to Json file
        if kwargs['debug']:
            if file_name:
                out_pth = str(file_name) + '_results.json'
            else:
                out_pth = str(image_name) + '_results.json'
            with open(out_pth, 'w') as f:
                json.dump(final_json, f, indent=4)
        print("elapsed time : {}s".format(time.time() - t))

        return final_json
    except IndexError as er:
        print("error occured while Recognising the text")
        print("error is ", er)
        sys.exit(1)
        #workers = 2 * number_of_cores + 1
#gunicorn --workers=9 --threads=9  --reload --bind 0.0.0.0:8081 --capture-output --error-logfile error_log.txt --access-logfile log.txt -k uvicorn.workers.UvicornWorker -t 300  --max-requests 1 --max-requests-jitter 5  main:app
#pkill gunicorn for killing the gunicorn process
if __name__ == "__main__":
    uvicorn.run('main:app', host='0.0.0.0', port=8080)

